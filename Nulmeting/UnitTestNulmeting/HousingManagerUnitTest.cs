using Xunit;
using Nulmeting;
using System.Collections.Generic;

namespace UnitTestNulmeting
{
    public class HousingManagerUnitTest
    {
        [Fact]
        public void AddHouse()
        {
            List<Falcilities> falcilities = new List<Falcilities> { Falcilities.Bath };
            HousingManager housing = new HousingManager();
            housing.AddHouse(HousingType.Bungalow,Theme.None, 10, falcilities, 7);

            Assert.Single(housing.Housings);
        }

        [Fact]
        public void AddMutipleHouses()
        {
            List<Falcilities> falcilities = new List<Falcilities> { Falcilities.Bath };
            HousingManager housing = new HousingManager();
            housing.AddHouse(HousingType.Bungalow,Theme.None, 10, falcilities, 7);
            housing.AddHouse(HousingType.HotelRoom, Theme.None, 9, falcilities, 2);
            housing.AddHouse(HousingType.HotelRoom,Theme.None, 12, falcilities, 2);

            Assert.Equal(3, housing.Housings.Count);
        }

        [Fact]
        public void DeleteHousingtest()
        {
            List<Falcilities> falcilities = new List<Falcilities> { Falcilities.Bath };
            HousingManager housing = new HousingManager();
            housing.AddHouse(HousingType.Bungalow, Theme.None, 10, falcilities, 7);
            housing.AddHouse(HousingType.HotelRoom, Theme.None, 9, falcilities, 2);
            housing.AddHouse(HousingType.HotelRoom, Theme.None, 12, falcilities, 2);
            Housing house = new Housing(2, HousingType.HotelRoom, Theme.None, 9, falcilities, 2);
            housing.RemoveHousing(house);
            Housing house1 = new Housing(1, HousingType.Bungalow, Theme.None, 10, falcilities, 7);
            Housing house2 = new Housing(3, HousingType.HotelRoom, Theme.None, 12, falcilities, 2);
            bool deletecheck = true;
            foreach(Housing X in housing.Housings)
            {
                if(X == house1)
                {
                    deletecheck = false;
                }
                if(X == house2)
                {
                    deletecheck = false;
                }
            }
            Assert.Equal(2, housing.Housings.Count);
            Assert.True(deletecheck);

        }

        [Fact]
        public void DetermineIdCheck()
        {
            bool idCheck = true;
            int id = 1;
            HousingManager housing = new HousingManager();
            List<Falcilities> falcilities = new List<Falcilities> { Falcilities.Bath, Falcilities.Shower };
            housing.AddHouse(HousingType.Bungalow, Theme.None, 20, falcilities, 4);
            housing.AddHouse(HousingType.Bungalow, Theme.None, 20, falcilities, 4);
            housing.AddHouse(HousingType.Bungalow, Theme.None, 20, falcilities, 4);
            foreach(Housing House in housing.Housings)
            {
                if(id != House.ID)
                {
                    idCheck = false;
                }
                id++;
            }

            Assert.True(idCheck);
        }

    }
}

﻿using Xunit;
using Nulmeting;
using System.Collections.Generic;
using System;

namespace UnitTestNulmeting
{
    public class ReservationManagerUnitTest
    {
        [Fact]
        public void AddNewReservationTest()
        {
            ReservationManager manager = new ReservationManager();
            List<Services> services = new List<Services> { Services.CleanBedSheets };
            var startdate = new DateTime(2020, 2, 18);
            var EndDate = new DateTime(2020, 7, 11);
            manager.AddReservation("Jeff",3,services,startdate,EndDate, HousingType.Bungalow);

            Assert.Single(manager.Reservations);
        } 
        
        [Fact]
        public void AddMutipleReservations()
        {
            ReservationManager manager = new ReservationManager();
            List<Services> services = new List<Services> { Services.CleanBedSheets };
            var startdate = new DateTime(2020, 2, 18);
            var EndDate = new DateTime(2020, 7, 11);

            manager.AddReservation("Jeff", 7, services, startdate, EndDate, HousingType.HotelRoom);
            manager.AddReservation("Derp", 4, services, startdate, EndDate, HousingType.HotelRoom);
            manager.AddReservation("Dan", 7, services, startdate, EndDate, HousingType.Bungalow);

            Assert.Equal(3, manager.Reservations.Count);
        }

        [Fact]
        public void DetermineIdTest()
        {
            bool idCheck = true;
            int id = 1;
            ReservationManager manager = new ReservationManager();
            List<Services> services = new List<Services> { Services.CleanBedSheets };
            var startdate = new DateTime(2020, 2, 18);
            var EndDate = new DateTime(2020, 7, 11);

            manager.AddReservation("Jeff", 7, services, startdate, EndDate, HousingType.Bungalow);
            manager.AddReservation("Dave", 3, services, startdate, EndDate, HousingType.CampingSpot);
            manager.AddReservation("louis", 5, services, startdate, EndDate, HousingType.CampingSpot);
            foreach (Reservation reservation in manager.Reservations)
            {
                if (id != reservation.ReservationId)
                {
                    idCheck = false;
                }
                id++;
            }

            Assert.True(idCheck);
        }

        [Fact]
        public void RemoveReservationCheck()
        {
            List<Falcilities> falcilities = new List<Falcilities> { Falcilities.Bath };
            ReservationManager manager = new ReservationManager();
            List<Services> services = new List<Services> { Services.CleanBedSheets };
            var startdate = new DateTime(2020, 2, 18);
            var EndDate = new DateTime(2020, 7, 11);
            manager.AddReservation("Jeff", 7, services, startdate, EndDate, HousingType.Bungalow);
            manager.AddReservation("Dave", 3, services, startdate, EndDate, HousingType.Bungalow);
            manager.AddReservation("louis", 5, services, startdate, EndDate, HousingType.Bungalow);
            Reservation reservation = new Reservation(2,"Dave", 2, 4,services, startdate, EndDate);
            manager.RemoveReservation(reservation);
            Reservation reservation1 = new Reservation(1,"Jeff",1, 7, services, startdate, EndDate);
            Reservation house2 = new Reservation(3,"louis", 3,5, services, startdate, EndDate);
            bool deletecheck = true;
            foreach (Reservation X in manager.Reservations)
            {
                if (X == reservation1)
                {
                    deletecheck = false;
                }
                if (X == house2)
                {
                    deletecheck = false;
                }
            }
            Assert.Equal(2, manager.Reservations.Count);
            Assert.True(deletecheck);
        }
        [Fact]
        public void SelectingHouingtest()
        {
            List<Falcilities> falcilities = new List<Falcilities> { Falcilities.Bath };
            HousingManager housing = new HousingManager();
            housing.AddHouse(HousingType.Bungalow, Theme.None, 10, falcilities, 7);
            housing.AddHouse(HousingType.HotelRoom, Theme.None, 9, falcilities, 2);
            housing.AddHouse(HousingType.HotelRoom, Theme.None, 12, falcilities, 2);
            ReservationManager manager = new ReservationManager();
            List<Housing> housings = new List<Housing>();
            housings = manager.HouseSorting(HousingType.HotelRoom);
            Assert.Equal(2,housings.Count);
        }
    }
}

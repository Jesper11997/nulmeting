﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nulmeting
{
    public class ReservationManager
    {
        public List<Reservation> Reservations { get; set; }
        private HousingManager Housing;
        //Voor test redenen
        private int HousingId = 1;

        public void AddReservation(string name,int people,List<Services>services, DateTime startDate, DateTime EndDate, HousingType type)
        {
            int NewId = DetermineReservationId();
            if(NewId == 1)
            {
                Reservations = new List<Reservation>();
            }
            //Algorithem om een een huis te plaatsen


            Reservation reservation = new Reservation(NewId, name, AppointHousing(people, type), people, services, startDate, EndDate);
            Reservations.Add(reservation);
            HousingId++;
        }

        private int DetermineReservationId()
        {
            int id;
            if (Reservations == null)
            {
                return 1;
            }
            Reservation reservation = Reservations.Last();
            id = reservation.ReservationId;
            id++;
            return id;
        }

        public void RemoveReservation(Reservation reservation)
        {
            int index = 0;
            for (int i = 0; i < Reservations.Count; i++)
            {
                if (Reservations[i] == reservation)
                {
                    index = i;
                    break;
                }
            }
            Reservations.RemoveAt(index);
        }

        public int AppointHousing(int AmountOfPeople, HousingType type)
        {
            if(Reservations == null)
            {

            }
            return HousingId;
        }

        public List<Housing> HouseSorting(HousingType type)
        {
            List<Housing> housings = new List<Housing>();
            foreach(Housing housing in HousingManager.housing.Housings)
            {
                if (housing.Type == type)
                {
                    housings.Add(housing);
                }
            }
            return housings;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Nulmeting
{
    public class HousingManager
    {
        public List<Housing> Housings { get; set; }
        public static HousingManager housing;

        public void AddHouse(HousingType type,Theme theme, int surface, List<Falcilities> falcilities, int MaxPeople)
        {
            int Id = DetermineId();
            if(Id == 1)
            {
                Housings = new List<Housing>();
            }
            Housing housing = new Housing(Id, type,theme, surface, falcilities, MaxPeople);
            Housings.Add(housing);
        }

        private int DetermineId()
        {
            int id;
            if(Housings == null)
            {
                return 1;
            }
            Housing house = Housings.Last();
            id = house.ID;
            id++;
            return id;
        }

        public void RemoveHousing(Housing house)
        {
            int index = 0;
            for (int i = 0; i < Housings.Count; i++)
            {
                if (Housings[i] == house)
                {
                    index = i;
                    break;
                }
            }
            Housings.RemoveAt(index);
        }
    }
}

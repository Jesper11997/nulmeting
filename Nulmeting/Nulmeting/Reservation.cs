﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nulmeting
{
    public class Reservation
    {
        public int ReservationId { get; }
        public string Name { get; }
        public int HousingId { get; set; }
        public int People { get; set; }
        public List<Services> Services { get; }
        public DateTime StartDate { get; set;}
        public DateTime EndDate { get; set; }

        public Reservation(int reservationId, string name, int housingId, int people, List<Services> services, DateTime startDate, DateTime endDate)
        {
            ReservationId = reservationId;
            Name = name;
            HousingId = housingId;
            People = people;
            Services = services;
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}

﻿namespace Nulmeting
{
    public enum HousingType
    {
        Bungalow,
        HotelRoom,
        CampingSpot
    }

    public enum Theme
    {
        None,
        SuperMario,
        Pirate,
        Jungle,
    }

    public enum Falcilities
    {
        Shower,
        Bath,
        Wifi,
        MiniBar
    }

    public enum Services
    {
        None,
        CleanBedSheets,
        ChildsBed
    }
}

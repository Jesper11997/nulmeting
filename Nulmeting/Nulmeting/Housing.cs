﻿using System.Collections.Generic;

namespace Nulmeting
{
    public class Housing
    {


        public int ID { get; }
        public HousingType Type { get; }
        public Theme Theme { get; }
        public int Surface { get; }
        public List<Falcilities> Falcilities { get; }
        public int MaxPeople { get; }
        public bool BigFailures { get; }
        public string Failure { get; set; }

        public Housing(int iD, HousingType type, Theme theme,int surface, List<Falcilities> falcilities, int maxPeople)
        {
            ID = iD;
            Theme = theme;
            Type = type;
            this.Surface = surface;
            this.Falcilities = falcilities;
            MaxPeople = maxPeople;
            BigFailures = false;
        }

    }
}
